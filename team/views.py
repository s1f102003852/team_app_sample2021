from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
import urllib
import json

from .models import Reply

WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = 'czsSTF3rbh4SBfx5kZ50swbD'
ACTION_HOW_ARE_YOU = 'HOW_ARE_YOU'

JR_API_LINE = ""
JR_API_URL = "https://api-tokyochallenge.odpt.org/api/v4/odpt:TrainInformation?odpt:operator=odpt.Operator:JR-East&odpt:railway=odpt.Railway:JR-East."+JR_API_LINE+"&acl:consumerKey=E8-L-t7sD8QmDDrDzfgxIm_c2gwT4hnFvtTb4hd-GkY"

def index(request):
    kehin_replies = Reply.objects.filter(response=Reply.Kehin)
    saikyo_replies = Reply.objects.filter(response=Reply.Saikyo)
    syonan_replies = Reply.objects.filter(response=Reply.Syonan)
    takasaki_replies = Reply.objects.filter(response=Reply.Takasaki)
    utsunomiya_replies = Reply.objects.filter(response=Reply.Utsunomiya)




    context = {
        'kehin_replies': kehin_replies,
        'saikyo_replies': saikyo_replies,
        'syonan_replies': syonan_replies,
        'takasaki_replies' : takasaki_replies,
        'utsunomiya_replies' : utsunomiya_replies,

    }
    return render(request, 'index.html', context)

def post_message(url, data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(url, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()


def clear(request):
    Reply.objects.all().delete()
    return redirect(index)

def announce(request):
    if request.method == 'POST':
        data = {
            'text': request.POST['message']
        }
        post_message(WEBHOOK_URL, data)

    return redirect(index)


@csrf_exempt
def echo(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'text': '<@{}> {}'.format(user_id, content.upper()),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def hello(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> How are you?'.format(user_id)
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': 'I am:',
                        'emoji': True
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Fine.',
                                'emoji': True
                            },
                            'value': 'positive'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'So so.',
                                'emoji': True
                            },
                            'value': 'neutral'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Terrible.',
                                'emoji': True
                            },
                            'value': 'negative'
                        }
                    ],
                    'action_id': ACTION_HOW_ARE_YOU
                }
            }
        ],
        'response_type': 'in_channel'
    }

    return JsonResponse(result)
"""
@csrf_exempt
def reply(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    payload = json.loads(request.POST.get('payload'))
    print(payload)
    if payload.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    if payload['actions'][0]['action_id'] != ACTION_HOW_ARE_YOU:
        raise SuspiciousOperation('Invalid request.')
    
    user = payload['user']
    selected_value = payload['actions'][0]['selected_option']['value']
    response_url = payload['response_url']

    if selected_value == 'positive':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.POSITIVE)
        reply.save()
        response = {
            'text': '<@{}> Great! :smile:'.format(user['id'])
        }
    elif selected_value == 'neutral':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
        reply.save()
        response = {
            'text': '<@{}> Ok, thank you! :sweat_smile:'.format(user['id'])
        }
    else:
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEGATIVE)
        reply.save()
        response = {
            'text': '<@{}> Good luck! :innocent:'.format(user['id'])
        }
    
    post_message(response_url, response)

    return JsonResponse({})
"""
#ここから開発しているもの

@csrf_exempt
def train(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> Which train do you choice?'.format(user_id)
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': 'Which line ...?:',
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Kehin Tohoku line(京浜東北線)',
                            },
                            'value': 'Kehin'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Saikyo line(埼京線)',
                            },
                            'value': 'Saikyo'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Syonan Sinjyuku line(湘南新宿ライン)',
                            },
                            'value': 'Syonan'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Takasaki line(高崎線)',
                            },
                            'value': 'Takasaki'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Utsunomiya line(宇都宮線)',
                            },
                            'value': 'Utsunomiya'
                        }
                    ],
                    'action_id': ACTION_HOW_ARE_YOU
                }
            }
        ],
        'response_type': 'in_channel'
    }
    
    return JsonResponse(result)

@csrf_exempt
def reply(request):
    if request.method != 'POST':
        return JsonResponse({})

    payload = json.loads(request.POST.get('payload'))
    print(payload)
    if payload.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    if payload['actions'][0]['action_id'] != ACTION_HOW_ARE_YOU:
        raise SuspiciousOperation('Invalid request.')

    user = payload['user']
    selected_value = payload['actions'][0]['selected_option']['value']
    response_url = payload['response_url']

    if selected_value == 'Kehin':
        JR_API_LINE = "KeihinTohokuNegishi"
        JR_API_URL = "https://api-tokyochallenge.odpt.org/api/v4/odpt:TrainInformation?odpt:operator=odpt.Operator:JR-East&odpt:railway=odpt.Railway:JR-East."+JR_API_LINE+"&acl:consumerKey=E8-L-t7sD8QmDDrDzfgxIm_c2gwT4hnFvtTb4hd-GkY"
        req = urllib.request.Request(JR_API_URL)
        res = urllib.request.urlopen(req)
        json_body = str(json.loads(res.read().decode('utf-8')))
        json_body = eval(json_body[1:-1])
        info = json_body['odpt:trainInformationText']['ja']
        res.close()
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.Kehin)
        reply.save()
        response = {
            'text': '<@{0}>  京浜東北線：{1}'.format(user['id'],info)
        }
    elif selected_value == 'Saikyo':
        JR_API_LINE = "SaikyoKawagoe"
        JR_API_URL = "https://api-tokyochallenge.odpt.org/api/v4/odpt:TrainInformation?odpt:operator=odpt.Operator:JR-East&odpt:railway=odpt.Railway:JR-East."+JR_API_LINE+"&acl:consumerKey=E8-L-t7sD8QmDDrDzfgxIm_c2gwT4hnFvtTb4hd-GkY"
        req = urllib.request.Request(JR_API_URL)
        res = urllib.request.urlopen(req)
        json_body = str(json.loads(res.read().decode('utf-8')))
        json_body = eval(json_body[1:-1])
        info = json_body['odpt:trainInformationText']['ja']
        res.close()
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.Kehin)
        reply.save()
        response = {
            'text': '<@{0}>  埼京線：{1}'.format(user['id'],info)
        }
    elif selected_value == 'Syonan':
        JR_API_LINE = "ShonanShinjuku"
        JR_API_URL = "https://api-tokyochallenge.odpt.org/api/v4/odpt:TrainInformation?odpt:operator=odpt.Operator:JR-East&odpt:railway=odpt.Railway:JR-East."+JR_API_LINE+"&acl:consumerKey=E8-L-t7sD8QmDDrDzfgxIm_c2gwT4hnFvtTb4hd-GkY"
        req = urllib.request.Request(JR_API_URL)
        res = urllib.request.urlopen(req)
        json_body = str(json.loads(res.read().decode('utf-8')))
        json_body = eval(json_body[1:-1])
        info = json_body['odpt:trainInformationText']['ja']
        res.close()
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.Kehin)
        reply.save()
        response = {
            'text': '<@{0}>  湘南新宿ライン：{1}'.format(user['id'],info)
        }
    elif selected_value == 'Takasaki':
        JR_API_LINE = "Takasaki"
        JR_API_URL = "https://api-tokyochallenge.odpt.org/api/v4/odpt:TrainInformation?odpt:operator=odpt.Operator:JR-East&odpt:railway=odpt.Railway:JR-East."+JR_API_LINE+"&acl:consumerKey=E8-L-t7sD8QmDDrDzfgxIm_c2gwT4hnFvtTb4hd-GkY"
        req = urllib.request.Request(JR_API_URL)
        res = urllib.request.urlopen(req)
        json_body = str(json.loads(res.read().decode('utf-8')))
        json_body = eval(json_body[1:-1])
        info = json_body['odpt:trainInformationText']['ja']
        res.close()
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.Kehin)
        reply.save()
        response = {
            'text': '<@{0}>  高崎線：{1}'.format(user['id'],info)
        }
    else:
        JR_API_LINE = "Utsunomiya"
        JR_API_URL = "https://api-tokyochallenge.odpt.org/api/v4/odpt:TrainInformation?odpt:operator=odpt.Operator:JR-East&odpt:railway=odpt.Railway:JR-East."+JR_API_LINE+"&acl:consumerKey=E8-L-t7sD8QmDDrDzfgxIm_c2gwT4hnFvtTb4hd-GkY"
        req = urllib.request.Request(JR_API_URL)
        res = urllib.request.urlopen(req)
        json_body = str(json.loads(res.read().decode('utf-8')))
        json_body = eval(json_body[1:-1])
        info = json_body['odpt:trainInformationText']['ja']
        res.close()
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.Kehin)
        reply.save()
        response = {
            'text': '<@{0}>  宇都宮線：{1}'.format(user['id'],info)
        }

    post_message(response_url, response)
    
    return JsonResponse({})