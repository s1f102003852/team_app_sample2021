from django.db import models

class Reply(models.Model):
    Kehin = 1
    Saikyo = 2
    Syonan = 3
    Takasaki = 4
    Utsunomiya = 5

    user_name = models.CharField(max_length=100)
    user_id = models.CharField(max_length=100)
    response = models.IntegerField(default=Saikyo)